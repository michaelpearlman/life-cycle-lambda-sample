```
#!txt

Copyright 2016 Reltio 100 Marine Parkway, Suite 275, Redwood Shores, CA USA 94065 (855) 360-DATA www.reltio.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```
  
[TOC]




# Welcome

Welcome to the project - Reltio LCA Lambda Samples!

## Purposes

We have prepared for you the samples of the Reltio LCA Lambda implementations in order to help in the start point of the LCA service usage.

## Overview

The Life Cycle Actions Framework (LCAF) provides an easy way to build and deploy custom logic for creating, updating, deleting and performing other operations such as controlling passed data or the prevention of operations. You can apply custom LCAF logic to entities, relationships, interactions, and graphs.
