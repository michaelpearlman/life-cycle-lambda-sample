package media;

import com.reltio.lifecycle.framework.*;
import com.reltio.lifecycle.lambda.LifeCycleActionHandler;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by michaelpearlman on 7/20/16.
 */
public class filmDQ extends LifeCycleActionHandler {
    private final Logger logger = Logger.getLogger(filmDQ.class.getName());


    @Override
    public ILifeCycleObjectData beforeSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
        IObject object = data.getObject();
        if (object == null) {
            return null;
        }
        IAttributes attributes = object.getAttributes();
        if (attributes == null) {
            return null;
        }
        ICrosswalks crosswalks = object.getCrosswalks();
        if (crosswalks == null || crosswalks.getCrosswalks().isEmpty()) {
            return null;
        }

        String uri = object.getUri();


        try {

            List<ICrosswalk> crosswalksCopy = new ArrayList<>(crosswalks.getCrosswalks());
            for (ICrosswalk cw : crosswalksCopy) {
                if (cw.getType().equals("configuration/sources/DQService")) {

                    crosswalks.removeCrosswalk(cw);
                }
            }


            ICrosswalk dqCrosswalk = crosswalks.createCrosswalk().value(uri.substring(uri.indexOf('/') + 1)).type("configuration/sources/DQService").build();
            crosswalks.addCrosswalk(dqCrosswalk);


            boolean updated = false;
            boolean addBV = false;

            int dqScore = 100;
            float bvScore1 = 0;
            float earningsBV = 1f;
            float dateBV = 1f;


            List<IAttributeValue> genreValues = attributes.getAttributeValues("Genre");
            if (!genreValues.isEmpty()) {
                for (IAttributeValue genreValue : genreValues) {
                    INestedAttributeValue genre = (INestedAttributeValue) genreValue;
                    List<IAttributeValue> genreValueValues = genre.getValue().getAttributeValues("genre");
                    for (IAttributeValue rawValue : genreValueValues) {
                        ISimpleAttributeValue rawValue1 = (ISimpleAttributeValue) rawValue;
                        String attributeValue = rawValue1.getStringValue();
                        if (genreValueValues == null || genreValueValues.isEmpty()) {
                            INestedAttributeValue n = attributes.createNestedAttributeValue("DQException").uri(uri + "/attributes/DQException/5")
                                    .simpleAttribute("Attribute", "Genre")
                                    .simpleAttribute("Problem", "Missing")
                                    .build();
                            attributes.addAttributeValue(n);
                            dqCrosswalk.getAttributes().addAttributeURI(n.getUri());
                            dqCrosswalk.getAttributes().addAttributeURI(n.getValue().getAllAttributeValues().get(0).getUri());
                            dqCrosswalk.getAttributes().addAttributeURI(n.getValue().getAllAttributeValues().get(1).getUri());
                            dqScore -= 5;
                            updated = true;

                        }
                    }
                }
            } else {
                INestedAttributeValue n = attributes.createNestedAttributeValue("DQException").uri(uri + "/attributes/DQException/4")
                        .simpleAttribute("Attribute", "Genre")
                        .simpleAttribute("Problem", "Missing")
                        .build();
                attributes.addAttributeValue(n);
                dqCrosswalk.getAttributes().addAttributeURI(n.getUri());
                dqCrosswalk.getAttributes().addAttributeURI(n.getValue().getAllAttributeValues().get(0).getUri());
                dqCrosswalk.getAttributes().addAttributeURI(n.getValue().getAllAttributeValues().get(1).getUri());
                dqScore -= 10;
                updated = true;
            }

            List<IAttributeValue> grossEarnList = attributes.getAttributeValues("grossEarn");
            if (grossEarnList.isEmpty()) {
                INestedAttributeValue n = attributes.createNestedAttributeValue("DQException").uri(uri + "/attributes/DQException/3")
                        .simpleAttribute("Attribute", "Gross Earnings")
                        .simpleAttribute("Problem", "Missing")
                        .build();
                attributes.addAttributeValue(n);
                dqCrosswalk.getAttributes().addAttributeURI(n.getUri());
                dqCrosswalk.getAttributes().addAttributeURI(n.getValue().getAllAttributeValues().get(0).getUri());
                dqCrosswalk.getAttributes().addAttributeURI(n.getValue().getAllAttributeValues().get(1).getUri());
                dqScore -= 10;
                updated = true;
            } else {
                ISimpleAttributeValue grossEarnings = (ISimpleAttributeValue) grossEarnList.get(0);
                String earningsValue = grossEarnings.getStringValue();
                int grossEarn = Integer.parseInt(earningsValue.replaceAll("[\\D]", ""));

                if (grossEarn <= 50000000) {
                    earningsBV -= .7;
                }
                if (grossEarn > 50000000 && grossEarn <= 150000000) {
                    earningsBV -= .35;
                }
                if (grossEarn > 150000000 && grossEarn <= 250000000) {
                    earningsBV -= .15;
                }
                if (grossEarn > 250000000 && grossEarn <= 300000000) {
                    earningsBV += .01;
                }
                if (grossEarn > 300000000 && grossEarn <= 400000000) {
                    earningsBV += .1;
                }
                if (grossEarn > 400000000 && grossEarn <= 500000000) {
                    earningsBV += .2;
                }
                if (grossEarn > 500000000 && grossEarn <= 600000000) {
                    earningsBV += .3;
                }
                if (grossEarn > 600000000 && grossEarn <= 700000000) {
                    earningsBV += .4;
                }
                if (grossEarn > 700000000 && grossEarn <= 800000000) {
                    earningsBV += .5;
                }
                if (grossEarn > 800000000 && grossEarn <= 900000000) {
                    earningsBV += .6;
                }
                if (grossEarn > 900000000) {
                    earningsBV += .7;
                }
                addBV = true;
            }

            List<IAttributeValue> titleList = attributes.getAttributeValues("Title");
            if (titleList.isEmpty()) {
                INestedAttributeValue n = attributes.createNestedAttributeValue("DQException").uri(uri + "/attributes/DQException/1")
                        .simpleAttribute("Attribute", "Title")
                        .simpleAttribute("Problem", "Missing")
                        .build();
                attributes.addAttributeValue(n);
                dqCrosswalk.getAttributes().addAttributeURI(n.getUri());
                dqCrosswalk.getAttributes().addAttributeURI(n.getValue().getAllAttributeValues().get(0).getUri());
                dqCrosswalk.getAttributes().addAttributeURI(n.getValue().getAllAttributeValues().get(1).getUri());
                dqScore -= 10;
                updated = true;
            }

            List<IAttributeValue> releaseDateList = attributes.getAttributeValues("yearReleased");
            if (releaseDateList.isEmpty()) {
                INestedAttributeValue n = attributes.createNestedAttributeValue("DQException").uri(uri + "/attributes/DQException/2")
                        .simpleAttribute("Attribute", "Year Released")
                        .simpleAttribute("Problem", "Missing")
                        .build();
                attributes.addAttributeValue(n);
                dqCrosswalk.getAttributes().addAttributeURI(n.getUri());
                dqCrosswalk.getAttributes().addAttributeURI(n.getValue().getAllAttributeValues().get(0).getUri());
                dqCrosswalk.getAttributes().addAttributeURI(n.getValue().getAllAttributeValues().get(1).getUri());
                dqScore -= 10;
                updated = true;
            } else {
                ISimpleAttributeValue yearReleased = (ISimpleAttributeValue) releaseDateList.get(0);
                String yearValue = yearReleased.getStringValue();
                int yearDiff = dateScore(yearValue);
                if (yearDiff == 4) {
                    dateBV -= .05;
                }
                if (yearDiff > 4 && yearDiff <= 8) {
                    dateBV -= .1;
                }
                if (yearDiff > 8 && yearDiff <= 12) {
                    dateBV -= .15;
                }
                if (yearDiff > 12 && yearDiff <= 16) {
                    dateBV -= .2;
                }
                if (yearDiff > 16 && yearDiff <= 20) {
                    dateBV -= .25;
                }
                if (yearDiff > 20 && yearDiff <= 24) {
                    dateBV -= .3;
                }
                if (yearDiff > 24) {
                    dateBV -= .35;
                }
                addBV = true;
                updated = true;
            }

            bvScore1 = (dateBV * earningsBV) * 10;
            //System.out.println(bvScore);
            bvScore1 = Math.round(bvScore1);
            int bvScore = (int) bvScore1;

            if (bvScore < 0) {
                bvScore = 0;
            }
            if (bvScore > 10) {
                bvScore = 10;
            }

            INestedAttributeValue b = attributes.createNestedAttributeValue("OverallDQ").uri(uri + "/attributes/OverallDQ/10")
                    .simpleAttribute("perBusinessRules", dqScore)
                    .simpleAttribute("HighValue", "Good")
                    .build();
            attributes.addAttributeValue(b);
            dqCrosswalk.getAttributes().addAttributeURI(b.getUri());
            dqCrosswalk.getAttributes().addAttributeURI(b.getValue().getAllAttributeValues().get(0).getUri());
            dqCrosswalk.getAttributes().addAttributeURI(b.getValue().getAllAttributeValues().get(1).getUri());


            ISimpleAttributeValue m = attributes.createSimpleAttributeValue("BusinessValue").value(bvScore).uri(uri + "/attributes/BusinessValue/11").build();
                attributes.addAttributeValue(m);
                dqCrosswalk.getAttributes().addAttributeURI(m.getUri());



            if (updated) {
                return data;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            reltioAPI.logError(ex.getMessage());
            throw new RuntimeException("Film Demo DQ: LCA invokation failed. " + ex.getMessage());
        }
    }

    public static int dateScore(String releaseDate) {
        String currentTime = getCurrentTime();
        int yearDiff = 0;
        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
            DateTime oldDate = formatter.parseDateTime(releaseDate);
            DateTime newDate = formatter.parseDateTime(currentTime);
            Period per = new Period(oldDate.toLocalDateTime(), newDate.toLocalDateTime());
            yearDiff = per.getYears();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return yearDiff;
    }
    private static String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        TimeZone fromTimeZone = calendar.getTimeZone();
        TimeZone toTimeZone = TimeZone.getTimeZone("PST");

        calendar.setTimeZone(fromTimeZone);
        calendar.add(Calendar.MILLISECOND, fromTimeZone.getRawOffset() * -1);
        if (fromTimeZone.inDaylightTime(calendar.getTime())) {
            calendar.add(Calendar.MILLISECOND, calendar.getTimeZone()
                    .getDSTSavings() * -1);
        }

        calendar.add(Calendar.MILLISECOND, toTimeZone.getRawOffset());
        if (toTimeZone.inDaylightTime(calendar.getTime())) {
            calendar.add(Calendar.MILLISECOND, toTimeZone.getDSTSavings());
        }
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(calendar.getTime());
    }
}