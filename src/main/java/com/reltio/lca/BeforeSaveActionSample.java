package com.reltio.lca;

import com.reltio.lifecycle.framework.*;
import com.reltio.lifecycle.lambda.LifeCycleActionHandler;

import java.util.List;


public class BeforeSaveActionSample extends LifeCycleActionHandler {

    @Override
    public ILifeCycleObjectData beforeSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
        IObject object = data.getObject();
        IAttributes attributes = object.getAttributes();
        try {
            List<IAttributeValue> fullNames = attributes.getAttributeValues("Name");
            if (fullNames.isEmpty()) {
                List<IAttributeValue> firstNames = attributes.getAttributeValues("FirstName");
                List<IAttributeValue> lastNames = attributes.getAttributeValues("LastName");
                if (!firstNames.isEmpty() && !lastNames.isEmpty()) {
                    ISimpleAttributeValue firstName = (ISimpleAttributeValue) firstNames.get(0);
                    ISimpleAttributeValue lastName = (ISimpleAttributeValue) lastNames.get(0);
                    String firstNameValue = firstName.getStringValue();
                    String lastNameValue = lastName.getStringValue();
                    String fullName = firstNameValue + " " + lastNameValue;
                    ISimpleAttributeValue newValue = attributes.createSimpleAttributeValue("Name").value(fullName).build();
                    attributes.addAttributeValue(newValue);
                }
            }
            return data;
        } catch (Exception ex) {
            reltioAPI.logError(ex.getMessage());
            throw new RuntimeException("BeforeSaveActionSample: LCA invocation failed.");
        }
    }
}
